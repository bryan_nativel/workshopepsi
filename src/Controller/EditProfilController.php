<?php

namespace App\Controller;

use App\Form\EditProfilType;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class EditProfilController extends AbstractController
{
    /**
     * @Route("edit/profil", name="edit_profil")
     * @param Request $request
     * @param UserInterface $userProfile
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function editProfil(Request $request, UserInterface $userProfile, UserPasswordEncoderInterface $encoder): Response
    {
        $form = $this->createForm(EditProfilType::class,$userProfile); //On utiliser le userProfile pour générer le formulaire pré-rempli
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Pour encoder le password quand il est modifié
            $hash = $encoder->encodePassword($userProfile, $userProfile->getPassword());
            $userProfile->setPassword($hash);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userProfile);
            $entityManager->flush();
            return $this->redirectToRoute('profil');
        }
        if($userProfile->getType() === 'consumer') {
            return $this->render('edit_profil/edit_profil_consumer.twig', [
                'user' => $userProfile,
                'form' => $form->createView()
            ]);
        }else{
            return $this->render('edit_profil/edit_profil_producer.twig', [
                'user' => $userProfile,
                'form' => $form->createView()
            ]);
        }
    }
    public function getOrders($userProfile) {
        $orders = $userProfile -> getOrders();
        $ordersPayed = [];
        foreach ($orders as $key => $val){
            if($orders[$key]->getStatus() ===  "Commandé"){
                array_push($ordersPayed, $orders[$key]);
//                array_push($ordersPayed, (object)[
//                    $orders[$key]
//                ]);
            }
        }
        return $ordersPayed;
    }
//    /**
//     * @Route("order/profil", name="order_profile")
//     * @param Request $request
//     * @param UserInterface $userProfile
//     * @return Response
//     */
//    public function orderProfile(Request $request, UserInterface $userProfile): Response
//    {
//        $orderPayed = $this-> getOrders($userProfile);
////        $prods = null;
////        if($orderPayed) {
////            foreach ($orderPayed as $k => $val){
////                $prods = $orderPayed[$k] -> getOrderItems();
////            }
////        }
//        return $this->render('orders/orders_customer.html.twig', [
////            'cart_products' => $prods,
//            'orders' => $orderPayed
//        ]);
//
//    }

    /**
     * @Route("/profil/index", name="profil")
     * @param Request $request
     * @param UserInterface $userProfile
     * @param UserPasswordEncoderInterface $encoder
     * @param ProductRepository $product
     * @return Response
     */
    public function displayProfil(Request $request, UserInterface $userProfile, UserPasswordEncoderInterface $encoder,ProductRepository $product): Response
    {
        
        $products = $product->findBy(array('user' => $userProfile));
        $orderPayed = $this-> getOrders($userProfile);

        if($userProfile->getType() === 'consumer') {
            return $this->render('edit_profil/page_profil_consumer.twig', [
                'user' => $userProfile,
                'orders' => $orderPayed
            ]);
        }else{
            return $this->render('edit_profil/page_profil_producer.twig', [
                'user' => $userProfile,
                'product'=> $products
            ]);
        }
    }


}
